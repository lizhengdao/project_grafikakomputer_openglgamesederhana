#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>

#include "Player.h";

using namespace std;

class FinishLine
{
private:
	float Xmin;
	float Xmax;
	float Ymin;
	float Ymax;
public:	
	void draw();
	void setColor(float, float, float);
	void setSize(float, float, float, float);

	float getXmaxPos();
	float getXminPos();
	float getYmaxPos();
	float getYminPos();

	int detectCol(Player, Player);
};

