#include "collisionLight.h"

void collisionLight::setSizeLight(float xMin, float xMax, float yMin, float yMax) {
	minX = xMin;
	maxX = xMax;
	minY = yMin;
	maxY = yMax;
}

void collisionLight::draw() {
	glBegin(GL_POLYGON);
	glRotatef(0.0, 0.0, 0.0, 1.0);

	glVertex3f(minX, minY, 0.0);
	glVertex3f(maxX, minY, 0.0);
	glVertex3f(maxX, maxY, 0.0);
	glVertex3f(minX, maxY, 0.0);

	glEnd();
}