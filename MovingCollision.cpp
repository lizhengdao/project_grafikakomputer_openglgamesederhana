#include "MovingCollision.h"

void MovingCollision::setSize(float xmax, float xmin, float ymax, float ymin) {
	maxX = xmax;
	minX = xmin;
	maxY = ymax;
	minY = ymin;
}

int MovingCollision::DetectCol(Player player1, Player player2) {
	if (player1.getXmaxPos() > minX && player1.getXminPos() < maxX
		&& player1.getYminPos() < maxY && player1.getYmaxPos() > minY) {
		return 1;
	}

	if (player2.getXmaxPos() > minX && player2.getXminPos() < maxX
		&& player2.getYminPos() < maxY && player2.getYmaxPos() > minY) {
		return 2;
	}
	return 0;
}

void MovingCollision::draw() {
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 0.0);
	glVertex3f(minX, minY, 0.0);
	glVertex3f(maxX, minY, 0.0);
	glVertex3f(maxX, maxY, 0.0);
	glVertex3f(minX, maxY, 0.0);
	glEnd();
}

void MovingCollision::moveObject() {	
		if (up) {
			minY += 0.01;
			maxY += 0.01;
			glutPostRedisplay();

			if (maxY >= 100) {
				up = false;
			}
		}

		else {
			minY -= 0.01;
			maxY -= 0.01;
			glutPostRedisplay();

			if (minY <= 0) {
				up = true;
			}
		}
}
