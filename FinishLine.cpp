#include "FinishLine.h"

void FinishLine::draw() {
	glBegin(GL_POLYGON); //menggambar polygon dengan titik tertentu
	glVertex3f(Xmin, Ymin, 0.0); //membuat titik pada x = 20, y = 20 z = 0
	glVertex3f(Xmax, Ymin, 0.0); //membuat titik pada x = 80, y = 20 z = 0
	glVertex3f(Xmax, Ymax, 0.0); //membuat titik pada x = 80, y = 80 z = 0
	glVertex3f(Xmin, Ymax, 0.0); //membuat titik pada x = 20, y = 80 z = 0
	glEnd();
}

void FinishLine::setColor(float red, float green, float blue) {
	glColor3f(red, green, blue); //set warna merah
}

void FinishLine::setSize(float xmax, float xmin, float ymax, float ymin) {
	Xmax = xmax;
	Xmin = xmin;
	Ymax = ymax;
	Ymin = ymin;
}

int FinishLine::detectCol(Player player1, Player player2) {
	if (player1.getXmaxPos() > Xmin && player1.getXminPos() < Xmax
		&& player1.getYminPos() < Ymax && player1.getYmaxPos() > Ymin) {
		return 1;
	}

	if (player2.getXmaxPos() > Xmin && player2.getXminPos() < Xmax
		&& player2.getYminPos() < Ymax && player2.getYmaxPos() > Ymin) {
		return 2;
	}
	return 0;
}

float FinishLine::getXmaxPos() {
	return Xmax;
}

float FinishLine::getXminPos() {
	return Xmin;
}

float FinishLine::getYmaxPos() {
	return Ymax;
}

float FinishLine::getYminPos() {
	return Ymin;
}

