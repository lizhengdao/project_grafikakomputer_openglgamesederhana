#include "Player.h"

void Player::draw() {
	glBegin(GL_POLYGON);
	glVertex3f(minX, minY, 0.0); 
	glVertex3f(maxX, minY, 0.0); 
	glVertex3f(maxX, maxY, 0.0); 
	glVertex3f(minX, maxY, 0.0); 
	glEnd();
}

void Player::setPosition(float Xmin, float Xmax, float Ymin, float Ymax) {
	minX = Xmin;
	maxX = Xmax;
	minY = Ymin;
	maxY = Ymax;
}

void Player::setColor(float red, float green, float blue) {
	glColor3f(red, green, blue); //set warna merah
}

void Player::RestartPos(float posminX, float posmaxX, float posminY, float posmaxY) {
	maxX = posmaxX;
	minX = posminX;
	maxY = posmaxY;
	minY = posminY;	
}

void Player::moveLeft() {
	minX -= 0.5;
	maxX -= 0.5;
	glutPostRedisplay();
}

void Player::moveRight() {
	minX += 0.5;
	maxX += 0.5;
	glutPostRedisplay();
}

void Player::moveUp() {
	minY += 0.5;
	maxY += 0.5;
	glutPostRedisplay();
}

void Player::moveDown() {
	minY -= 0.5;
	maxY -= 0.5;
	glutPostRedisplay();
}

float Player::getXmaxPos() {
	return maxX;
}

float Player::getXminPos() {
	return minX;
}

float Player::getYmaxPos() {
	return maxY;
}

float Player::getYminPos() {
	return minY;
}

void Player::increaseScore() {
	score += 1;
}

int Player::getScore() {
	return score;
}


